package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "org_building")
public class BuildingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "coop_id")
    private int coopId;

    @Column(name = "street_id")
    private int streetId;

    @Column(name = "city_id")
    private int cityId;

    @Column(name = "building_num")
    private String buildingNum;

    @Column(name = "building_num_idx")
    private String buildingNumIdx;

    @Column(name = "zip")
    private String zip;

    @Column(name = "natural_order")
    private String naturalOrder;

    public BuildingEntity() {
    }

    public BuildingEntity(int id, int coopId, int streetId, int cityId, String buildingNum, String buildingNumIdx, String zip, String naturalOrder) {
        this.id = id;
        this.coopId = coopId;
        this.streetId = streetId;
        this.cityId = cityId;
        this.buildingNum = buildingNum;
        this.buildingNumIdx = buildingNumIdx;
        this.zip = zip;
        this.naturalOrder = naturalOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCoopId() {
        return coopId;
    }

    public void setCoopId(int coopId) {
        this.coopId = coopId;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getBuildingNumIdx() {
        return buildingNumIdx;
    }

    public void setBuildingNumIdx(String buildingNumIdx) {
        this.buildingNumIdx = buildingNumIdx;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getNaturalOrder() {
        return naturalOrder;
    }

    public void setNaturalOrder(String naturalOrder) {
        this.naturalOrder = naturalOrder;
    }

    @Override
    public String toString() {
        return "BuildingEntity{" +
                "id=" + id +
                ", coopId=" + coopId +
                ", streetId=" + streetId +
                ", cityId=" + cityId +
                ", buildingNum='" + buildingNum + '\'' +
                ", buildingNumIdx='" + buildingNumIdx + '\'' +
                ", zip='" + zip + '\'' +
                ", naturalOrder='" + naturalOrder + '\'' +
                '}';
    }
}
