package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "cmn_street")
public class StreetEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "street_type")
    private String streetType;

    @Column(name = "city_id")
    private int cityId;

    public StreetEntity() {
    }

    public StreetEntity(int id, String name, String cityType, int cityId) {
        this.id = id;
        this.name = name;
        this.streetType = cityType;
        this.cityId = cityId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "StreetEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", streetType='" + streetType + '\'' +
                ", cityId=" + cityId +
                '}';
    }
}
