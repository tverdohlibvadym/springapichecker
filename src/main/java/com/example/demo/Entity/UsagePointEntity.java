package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mtr_usage_point")
public class UsagePointEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "address")
    private String address;

    @Column(name = "description")
    private String description;

    @Column(name = "ref_key")
    private String refKey;

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "service_id")
    private int serviceId;

    @Column(name = "is_shared")
    private boolean isShared;

    @Column(name = "is_controls")
    private boolean isControls;

    @Column(name = "path")
    private long path;

    @Column(name = "is_for_leak")
    private boolean isForLeak;

    @Column(name = "is_commercial")
    private boolean isCommercial;

    public UsagePointEntity() {
    }

    public UsagePointEntity(int id, String address, String description, String refKey, int parentId, int serviceId, boolean isShared, boolean isControls, long path, boolean isForLeak, boolean isCommercial) {
        this.id = id;
        this.address = address;
        this.description = description;
        this.refKey = refKey;
        this.parentId = parentId;
        this.serviceId = serviceId;
        this.isShared = isShared;
        this.isControls = isControls;
        this.path = path;
        this.isForLeak = isForLeak;
        this.isCommercial = isCommercial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public boolean isShared() {
        return isShared;
    }

    public void setShared(boolean shared) {
        isShared = shared;
    }

    public boolean isControls() {
        return isControls;
    }

    public void setControls(boolean controls) {
        isControls = controls;
    }

    public long getPath() {
        return path;
    }

    public void setPath(long path) {
        this.path = path;
    }

    public boolean isForLeak() {
        return isForLeak;
    }

    public void setForLeak(boolean forLeak) {
        isForLeak = forLeak;
    }

    public boolean isCommercial() {
        return isCommercial;
    }

    public void setCommercial(boolean commercial) {
        isCommercial = commercial;
    }

    @Override
    public String toString() {
        return "UsagePointEntity{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", refKey='" + refKey + '\'' +
                ", parentId=" + parentId +
                ", serviceId=" + serviceId +
                ", isShared=" + isShared +
                ", isControls=" + isControls +
                ", path=" + path +
                ", isForLeak=" + isForLeak +
                ", isCommercial=" + isCommercial +
                '}';
    }
}
