package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "org_appartment")
public class ApartmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "building_id")
    private int buildingId;

    @Column(name = "owner_name")
    private String ownerName;

    @Column(name = "main_occupant_id")
    private Integer mainOccupantId;

    @Column(name = "appartment_num")
    private String apartmentNum;

    @Column(name = "key_for_ref")
    private String keyForRef;

    @Column(name = "natural_order")
    private String naturalOrder;

    public ApartmentEntity() {
    }

    public ApartmentEntity(int id, int buildingId, String ownerName, int mainOccupantId, String apartmentNum, String keyForRef, String naturalOrder) {
        this.id = id;
        this.buildingId = buildingId;
        this.ownerName = ownerName;
        this.mainOccupantId = mainOccupantId;
        this.apartmentNum = apartmentNum;
        this.keyForRef = keyForRef;
        this.naturalOrder = naturalOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getMainOccupantId() {
        return mainOccupantId;
    }

    public void setMainOccupantId(Integer mainOccupantId) {
        this.mainOccupantId = mainOccupantId;
    }

    public String getApartmentNum() {
        return apartmentNum;
    }

    public void setApartmentNum(String apartmentNum) {
        this.apartmentNum = apartmentNum;
    }

    public String getKeyForRef() {
        return keyForRef;
    }

    public void setKeyForRef(String keyForRef) {
        this.keyForRef = keyForRef;
    }

    public String getNaturalOrder() {
        return naturalOrder;
    }

    public void setNaturalOrder(String naturalOrder) {
        this.naturalOrder = naturalOrder;
    }

    @Override
    public String toString() {
        return "ApartmentEntity{" +
                "id=" + id +
                ", buildingId=" + buildingId +
                ", ownerName='" + ownerName + '\'' +
                ", mainOccupantId=" + mainOccupantId +
                ", apartmentNum='" + apartmentNum + '\'' +
                ", keyForRef='" + keyForRef + '\'' +
                ", naturalOrder='" + naturalOrder + '\'' +
                '}';
    }
}
