package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "blng_customer")
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "balance")
    private double balance;

    @Column(name = "ref_key")
    private String refKey;

    @Column(name = "ref_ext")
    private String refExt;

    @Column(name = "instance_id")
    private int instanceId;

    @Column(name = "status_id")
    private int statusId;

    @Column(name = "version")
    private int version;

    public CustomerEntity() {
    }

    public CustomerEntity(int id, String code, String name, String address, double balance, String refKey, String refExt, int instanceId, int statusId, int version) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.address = address;
        this.balance = balance;
        this.refKey = refKey;
        this.refExt = refExt;
        this.instanceId = instanceId;
        this.statusId = statusId;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getRefExt() {
        return refExt;
    }

    public void setRefExt(String refExt) {
        this.refExt = refExt;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "CustomerEntity{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", balance=" + balance +
                ", refKey='" + refKey + '\'' +
                ", refExt='" + refExt + '\'' +
                ", instanceId=" + instanceId +
                ", statusId=" + statusId +
                ", version=" + version +
                '}';
    }
}

