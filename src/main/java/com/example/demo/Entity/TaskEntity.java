package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "org_task")
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "task_type_id")
    private int taskTypeId;

    @Column(name = "checker_id")
    private Integer checkerId;

    @Column(name = "date_create")
    private String dateCreate;

    @Column(name = "date_finish")
    private String dateFinish;

    @Column(name = "date_assigned")
    private String dateAssigned;

    @Column(name = "customer_code")
    private String customerCode;

    @Column(name = "comments")
    private String comments;

    @Column(name = "status")
    private String status;

    public TaskEntity() {
    }

    public TaskEntity(int id, int taskTypeId, int checkerId, String dateCreate, String dateFinish, String dateAssigned, String customerCode, String comments, String status) {
        this.id = id;
        this.taskTypeId = taskTypeId;
        this.checkerId = checkerId;
        this.dateCreate = dateCreate;
        this.dateFinish = dateFinish;
        this.dateAssigned = dateAssigned;
        this.customerCode = customerCode;
        this.comments = comments;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public Integer getCheckerId() {
        return checkerId;
    }

    public void setCheckerId(Integer checkerId) {
        this.checkerId = checkerId;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getDateAssigned() {
        return dateAssigned;
    }

    public void setDateAssigned(String dateAssigned) {
        this.dateAssigned = dateAssigned;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TaskEntity{" +
                "id=" + id +
                ", taskTypeId=" + taskTypeId +
                ", checkerId=" + checkerId +
                ", dateCreate='" + dateCreate + '\'' +
                ", dateFinish='" + dateFinish + '\'' +
                ", dateAssigned='" + dateAssigned + '\'' +
                ", customerCode='" + customerCode + '\'' +
                ", comments='" + comments + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
