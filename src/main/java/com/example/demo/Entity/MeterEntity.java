package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mtr_meter")
public class MeterEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "manufactured_at")
    private String manufacturedAt;

    @Column(name = "model_id")
    private int modelId;

    @Column(name = "ref_key")
    private String refKey;

    public MeterEntity() {
    }

    public MeterEntity(int id, String serialNumber, String manufacturedAt, int modelId, String refKey) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.manufacturedAt = manufacturedAt;
        this.modelId = modelId;
        this.refKey = refKey;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturedAt() {
        return manufacturedAt;
    }

    public void setManufacturedAt(String manufacturedAt) {
        this.manufacturedAt = manufacturedAt;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    @Override
    public String toString() {
        return "MeterEntity{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", manufacturedAt='" + manufacturedAt + '\'' +
                ", modelId=" + modelId +
                ", refKey='" + refKey + '\'' +
                '}';
    }
}
