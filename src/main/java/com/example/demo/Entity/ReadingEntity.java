package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mtr_reading")
public class ReadingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "reading_on")
    private String readingOn;

    @Column(name = "value")
    private double value;

    @Column(name = "ref_key")
    private String refKey;

    @Column(name = "installation_id")
    private int installationId;

    @Column(name = "prev_reading_id")
    private Integer prevReadingId;

    @Column(name = "volume")
    private Double volume;

    @Column(name = "reading_type_id")
    private int readingTypeId;

    @Column(name = "created")
    private String created;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "consumption_id")
    private String consumptionId;

    public ReadingEntity() {
    }

    public ReadingEntity(int id, String readingOn, double value, String refKey, int installationId, Integer prevReadingId, Double volume, int readingTypeId, String created, String createdBy, String consumptionId) {
        this.id = id;
        this.readingOn = readingOn;
        this.value = value;
        this.refKey = refKey;
        this.installationId = installationId;
        this.prevReadingId = prevReadingId;
        this.volume = volume;
        this.readingTypeId = readingTypeId;
        this.created = created;
        this.createdBy = createdBy;
        this.consumptionId = consumptionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReadingOn() {
        return readingOn;
    }

    public void setReadingOn(String readingOn) {
        this.readingOn = readingOn;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public int getInstallationId() {
        return installationId;
    }

    public void setInstallationId(int installationId) {
        this.installationId = installationId;
    }

    public Integer getPrevReadingId() {
        return prevReadingId;
    }

    public void setPrevReadingId(Integer prevReadingId) {
        this.prevReadingId = prevReadingId;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public int getReadingTypeId() {
        return readingTypeId;
    }

    public void setReadingTypeId(int readingTypeId) {
        this.readingTypeId = readingTypeId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getConsumptionId() {
        return consumptionId;
    }

    public void setConsumptionId(String consumptionId) {
        this.consumptionId = consumptionId;
    }

    @Override
    public String toString() {
        return "ReadingEntity{" +
                "id=" + id +
                ", readingOn='" + readingOn + '\'' +
                ", value=" + value +
                ", refKey='" + refKey + '\'' +
                ", installationId=" + installationId +
                ", prevReadingId=" + prevReadingId +
                ", volume=" + volume +
                ", readingTypeId=" + readingTypeId +
                ", created='" + created + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", consumptionId='" + consumptionId + '\'' +
                '}';
    }
}
