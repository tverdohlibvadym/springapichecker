package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cmn_var_value4")
public class VarValueEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "var_id")
    private int varId;

    @Column(name = "ref_key")
    private String refKey;

    @Column(name = "value")
    private String value;

    @Column(name = "is_readonly")
    private boolean isReadonly;

    @Column(name = "active_from")
    private String activeFrom;

    @Column(name = "active_to")
    private String activeTo;

    public VarValueEntity() {
    }

    public VarValueEntity(int id, int varId, String refKey, String value, boolean isReadonly, String activeFrom, String activeTo) {
        this.id = id;
        this.varId = varId;
        this.refKey = refKey;
        this.value = value;
        this.isReadonly = isReadonly;
        this.activeFrom = activeFrom;
        this.activeTo = activeTo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVarId() {
        return varId;
    }

    public void setVarId(int varId) {
        this.varId = varId;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isReadonly() {
        return isReadonly;
    }

    public void setReadonly(boolean readonly) {
        isReadonly = readonly;
    }

    public String getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(String activeFrom) {
        this.activeFrom = activeFrom;
    }

    public String getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(String activeTo) {
        this.activeTo = activeTo;
    }

    @Override
    public String toString() {
        return "VarValueEntity{" +
                "id=" + id +
                ", varId=" + varId +
                ", refKey='" + refKey + '\'' +
                ", value='" + value + '\'' +
                ", isReadonly=" + isReadonly +
                ", activeFrom='" + activeFrom + '\'' +
                ", activeTo='" + activeTo + '\'' +
                '}';
    }
}
