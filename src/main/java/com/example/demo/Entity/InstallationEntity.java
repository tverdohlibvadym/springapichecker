package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mtr_installation")
public class InstallationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "active_from")
    private String activeFrom;

    @Column(name = "active_to")
    private String activeTo;

    @Column(name = "meter_id")
    private int meterId;

    @Column(name = "usage_point_id")
    private int usagePointId;

    public InstallationEntity() {
    }

    public InstallationEntity(int id, String activeFrom, String activeTo, int meterId, int usagePointId) {
        this.id = id;
        this.activeFrom = activeFrom;
        this.activeTo = activeTo;
        this.meterId = meterId;
        this.usagePointId = usagePointId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(String activeFrom) {
        this.activeFrom = activeFrom;
    }

    public String getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(String activeTo) {
        this.activeTo = activeTo;
    }

    public int getMeterId() {
        return meterId;
    }

    public void setMeterId(int meterId) {
        this.meterId = meterId;
    }

    public int getUsagePointId() {
        return usagePointId;
    }

    public void setUsagePointId(int usagePointId) {
        this.usagePointId = usagePointId;
    }

    @Override
    public String toString() {
        return "InstallationEntity{" +
                "id=" + id +
                ", activeFrom='" + activeFrom + '\'' +
                ", activeTo='" + activeTo + '\'' +
                ", meterId=" + meterId +
                ", usagePointId=" + usagePointId +
                '}';
    }
}
