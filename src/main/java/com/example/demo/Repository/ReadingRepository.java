package com.example.demo.Repository;

import com.example.demo.Entity.ReadingEntity;
import com.example.demo.Entity.VarValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReadingRepository extends JpaRepository<ReadingEntity, Long> {
    List<ReadingEntity> findAll();
}
