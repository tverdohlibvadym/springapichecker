package com.example.demo.Repository;

import com.example.demo.Entity.VarValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VarValueRepository extends JpaRepository<VarValueEntity, Long> {
    List<VarValueEntity> findAll();
}
