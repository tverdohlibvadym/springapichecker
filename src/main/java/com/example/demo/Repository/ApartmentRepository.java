package com.example.demo.Repository;

import com.example.demo.Entity.ApartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApartmentRepository extends JpaRepository<ApartmentEntity, Long> {
    List<ApartmentEntity> findAll();
}