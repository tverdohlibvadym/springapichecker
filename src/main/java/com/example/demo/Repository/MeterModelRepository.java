package com.example.demo.Repository;

import com.example.demo.Entity.MeterModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeterModelRepository extends JpaRepository<MeterModelEntity, Long> {
    List<MeterModelEntity> findAll();
}