package com.example.demo.Repository;

import com.example.demo.Entity.StreetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StreetRepository extends JpaRepository<StreetEntity, Long> {
    List<StreetEntity> findAll();
}
