package com.example.demo.Repository;

import com.example.demo.Entity.InstallationEntity;
import com.example.demo.Entity.StreetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InstallationRepository extends JpaRepository<InstallationEntity, Long> {
    List<InstallationEntity> findAll();
}
