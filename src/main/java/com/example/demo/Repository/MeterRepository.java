package com.example.demo.Repository;

import com.example.demo.Entity.MeterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeterRepository extends JpaRepository<MeterEntity, Long> {
    List<MeterEntity> findAll();
}
