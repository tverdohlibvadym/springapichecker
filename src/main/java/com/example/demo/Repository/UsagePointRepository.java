package com.example.demo.Repository;

import com.example.demo.Entity.UsagePointEntity;
import com.example.demo.Entity.VarValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsagePointRepository extends JpaRepository<UsagePointEntity, Long> {
    List<UsagePointEntity> findAll();
}
