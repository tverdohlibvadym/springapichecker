package com.example.demo.Repository;

import com.example.demo.Entity.CityEntity;
import com.example.demo.Entity.StreetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<CityEntity, Long> {
    List<CityEntity> findAll();
}