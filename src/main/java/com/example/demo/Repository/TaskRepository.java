package com.example.demo.Repository;

import com.example.demo.Entity.TaskEntity;
import com.example.demo.Entity.VarValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
    List<TaskEntity> findAll();
}
