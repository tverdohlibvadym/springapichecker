package com.example.demo.Controller;

import com.example.demo.Entity.ApartmentEntity;
import com.example.demo.Entity.BuildingEntity;
import com.example.demo.Entity.CityEntity;
import com.example.demo.Entity.CustomerEntity;
import com.example.demo.Entity.InstallationEntity;
import com.example.demo.Entity.MeterEntity;
import com.example.demo.Entity.MeterModelEntity;
import com.example.demo.Entity.ReadingEntity;
import com.example.demo.Entity.StreetEntity;
import com.example.demo.Entity.TaskEntity;
import com.example.demo.Entity.UsagePointEntity;
import com.example.demo.Entity.VarValueEntity;
import com.example.demo.Repository.ApartmentRepository;
import com.example.demo.Repository.BuildingRepository;
import com.example.demo.Repository.CityRepository;
import com.example.demo.Repository.CustomerRepository;
import com.example.demo.Repository.InstallationRepository;
import com.example.demo.Repository.MeterModelRepository;
import com.example.demo.Repository.MeterRepository;
import com.example.demo.Repository.ReadingRepository;
import com.example.demo.Repository.StreetRepository;
import com.example.demo.Repository.TaskRepository;
import com.example.demo.Repository.UsagePointRepository;
import com.example.demo.Repository.VarValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    CityRepository cityRepository;
    @Autowired
    StreetRepository streetRepository;
    @Autowired
    BuildingRepository buildingRepository;
    @Autowired
    ApartmentRepository apartmentRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    VarValueRepository varValueRepository;
    @Autowired
    InstallationRepository installationRepository;
    @Autowired
    MeterRepository meterRepository;
    @Autowired
    MeterModelRepository meterModelRepository;
    @Autowired
    ReadingRepository readingRepository;
    @Autowired
    UsagePointRepository usagePointRepository;
    @Autowired
    TaskRepository taskRepository;

    @GetMapping("/test")
    public String welcome() {
        return "Test API";
    }

    @GetMapping("/cities")
    public ResponseEntity<List<CityEntity>> getAllCities() {
        List<CityEntity> cityEntities = new ArrayList<>(cityRepository.findAll());

        if (cityEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(cityEntities, HttpStatus.OK);
    }

    @GetMapping("/streets")
    public ResponseEntity<List<StreetEntity>> getAllStreets() {
        List<StreetEntity> streetEntities = new ArrayList<>(streetRepository.findAll());

        if (streetEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(streetEntities, HttpStatus.OK);
    }

    @GetMapping("/buildings")
    public ResponseEntity<List<BuildingEntity>> getAllBuildings() {
        List<BuildingEntity> buildingEntities = new ArrayList<>(buildingRepository.findAll());

        if (buildingEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(buildingEntities, HttpStatus.OK);
    }

    @GetMapping("/apartments")
    public ResponseEntity<List<ApartmentEntity>> getAllApartments() {
        List<ApartmentEntity> apartmentEntities = new ArrayList<>(apartmentRepository.findAll());

        if (apartmentEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(apartmentEntities, HttpStatus.OK);
    }

    @GetMapping("/customers")
    public ResponseEntity<List<CustomerEntity>> getAllCustomers() {
        List<CustomerEntity> customerEntities = new ArrayList<>(customerRepository.findAll());

        if (customerEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(customerEntities, HttpStatus.OK);
    }

    @GetMapping("/varvalues")
    public ResponseEntity<List<VarValueEntity>> getAllVarValues() {
        List<VarValueEntity> varValueEntities = new ArrayList<>(varValueRepository.findAll());

        if (varValueEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(varValueEntities, HttpStatus.OK);
    }

    @GetMapping("/installations")
    public ResponseEntity<List<InstallationEntity>> getAllInstallations() {
        List<InstallationEntity> installationEntities = new ArrayList<>(installationRepository.findAll());

        if (installationEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(installationEntities, HttpStatus.OK);
    }

    @GetMapping("/meters")
    public ResponseEntity<List<MeterEntity>> getAllMeters() {
        List<MeterEntity> meterEntities = new ArrayList<>(meterRepository.findAll());

        if (meterEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(meterEntities, HttpStatus.OK);
    }

    @GetMapping("/metermodels")
    public ResponseEntity<List<MeterModelEntity>> getAllMeterModels() {
        List<MeterModelEntity> meterModelEntities = new ArrayList<>(meterModelRepository.findAll());

        if (meterModelEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(meterModelEntities, HttpStatus.OK);
    }

    @GetMapping("/readings")
    public ResponseEntity<List<ReadingEntity>> getAllReadings() {
        List<ReadingEntity> readingEntities = new ArrayList<>(readingRepository.findAll());

        if (readingEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(readingEntities, HttpStatus.OK);
    }

    @GetMapping("/usagepoints")
    public ResponseEntity<List<UsagePointEntity>> getAllUsagePoints() {
        List<UsagePointEntity> usagePointEntities = new ArrayList<>(usagePointRepository.findAll());

        if (usagePointEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(usagePointEntities, HttpStatus.OK);
    }

    @GetMapping("/tasks")
    public ResponseEntity<List<TaskEntity>> getAllTasks() {
        List<TaskEntity> taskEntities = new ArrayList<>(taskRepository.findAll());

        if (taskEntities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(taskEntities, HttpStatus.OK);
    }
}
